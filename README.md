
# XMS Mesh Operation

## Requirements

1. XMS
2. SMTK ( https://gitlab.kitware.com/cmb/smtk )
4. CMake
5. Conan

## To Build
You will need to specify the following:
```
 SMTK_DIR = <SMTK-build or SMTK-install/lib/cmake/SMTK>
 ```
Then execute the following commands:
```
$ conan remote add aquaveo https://conan.aquaveo.com
$ conan install -pr <XMSMeshOperation-SOURCE_DIR>/dev/<PROFILE> <XMSMeshOperation-SOURCE_DIR> --build
$ cmake <XMSMeshOperation-SOURCE_DIR>
