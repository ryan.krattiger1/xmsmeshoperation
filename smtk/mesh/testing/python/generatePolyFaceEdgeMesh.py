#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

""" generatePolyFaceEdgeMesh.py:

Test/demonstrate the XMS mesh operation for face and edge mesh generation.

"""
import os
import sys
import unittest

import smtk
import smtk.attribute
import smtk.mesh
import smtk.model
import smtk.session.polygon
import smtk.testing

import smtkxmsmesh


class GeneratePolyFaceEdgeMesh(smtk.testing.TestCase):

    def setUp(self):
        self.mesh_points = None

        self.res_mananger = smtk.resource.Manager.create()
        self.op_manager = smtk.operation.Manager.create()

        smtk.attribute.Registrar.registerTo(self.res_mananger)
        smtk.attribute.Registrar.registerTo(self.op_manager)

        smtk.session.polygon.Registrar.registerTo(self.res_mananger)
        smtk.session.polygon.Registrar.registerTo(self.op_manager)

        smtk.mesh.xms.Registrar.registerTo(self.op_manager)

        smtk.operation.Registrar.registerTo(self.op_manager)
        self.op_manager.registerResourceManager(self.res_mananger)

    def read_model(self, model_path):
        loadOp = self.op_manager.createOperation(
            'smtk::session::polygon::LegacyRead')
        loadOp.parameters().find('filename').setValue(model_path)
        loadRes = loadOp.operate()
        outcome = loadRes.findInt('outcome').value()
        self.assertEqual(outcome, int(smtk.operation.Operation.SUCCEEDED))

        resource = loadRes.find('resource').value()
        model_resource = smtk.model.Resource.CastTo(resource)

        # Extract model from the resource
        uuids = model_resource.entitiesMatchingFlags(
            smtk.model.MODEL_ENTITY, True)
        model = model_resource.find(uuids.pop())

        return model

    def generateMesh(self, model):
        """"""
        # Create the "Generate Mesh" operator
        generateMeshOp = smtk.mesh.xms.GenerateMesh.create()

        # Associate it to the loaded model
        generateMeshOp.parameters().associate(model)
        generateMeshOp.parameters().findDouble('Sizing').setIsEnabled(True)
        generateMeshOp.parameters().findDouble('Sizing').setValue(0.4)

        # Execute the operation
        res = generateMeshOp.operate()
        outcome = res.find('outcome').value()
        self.assertEqual(outcome, int(
            smtk.operation.Operation.Outcome.SUCCEEDED))

        mesh_resource = res.find('resource').value()

        # Construct common set of mesh point ids
        self.mesh_points = mesh_resource.points()
        print('self.mesh_points:', self.mesh_points.size())

        print('meshes: %d\tcells: %d\tpoints: %d' % (mesh_resource.meshes().size(),
                                                     mesh_resource.cells().size(),
                                                     mesh_resource.points().size()))
        print('3d cells: %d\t2d cells: %d\t1d cells: %d\t0d cells: %d' %
              (mesh_resource.cells(smtk.mesh.Dims3).size(),
               mesh_resource.cells(smtk.mesh.Dims2).size(),
               mesh_resource.cells(smtk.mesh.Dims1).size(),
               mesh_resource.cells(smtk.mesh.Dims0).size()))

        return mesh_resource

    def check_classification(self, model, mesh_resource):
        """"""
        model_resource = smtk.model.Resource.CastTo(model.resource())
        tess = smtk.mesh.Tessellation(False, False)

        face_uuids = model_resource.entitiesMatchingFlags(
            smtk.model.FACE, True)
        for uuid in face_uuids:
            ent = model_resource.find(uuid)
            meshset = mesh_resource.findAssociatedMeshes(uuid, smtk.mesh.Dims2)
            face = smtk.model.Face(ent)
            print('face', face.name(), 'meshset cells:', meshset.cells().size())

            tess.extract(meshset, self.mesh_points)
            conn = tess.connectivity()
            self.assertTrue(sum(conn) > 0)
            self.assertTrue(min(conn) >= 0)
            self.assertTrue(max(conn) < self.mesh_points.size())

        edge_uuids = model_resource.entitiesMatchingFlags(
            smtk.model.EDGE, True)
        for uuid in edge_uuids:
            ent = model_resource.find(uuid)
            meshset = mesh_resource.findAssociatedMeshes(uuid, smtk.mesh.Dims1)
            edge = smtk.model.Face(ent)
            print('edge', edge.name(), 'meshset:', meshset.cells().size())
            if meshset.cells().size() == 0:
                print('Warning no mesh cells')  # this might be OK
                continue

            tess.extract(meshset, self.mesh_points)
            conn = tess.connectivity()
            self.assertTrue(sum(conn) > 0)
            self.assertTrue(min(conn) >= 0)
            self.assertTrue(max(conn) < self.mesh_points.size())

    def test(self):
        model_path = os.path.join(smtk.testing.DATA_DIR, 'test2D.smtk')
        print('model_path', model_path)
        self.assertTrue(os.path.exists(model_path))
        model = self.read_model(model_path)
        print('model', model)

        mesh_resource = self.generateMesh(model)
        print('mesh', mesh_resource)

        self.check_classification(model, mesh_resource)


if __name__ == '__main__':
    smtk.testing.process_arguments()
    unittest.main()
