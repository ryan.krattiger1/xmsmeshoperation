#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

""" generateFaceMesh.py:

Test/demonstrate the ability to use the XMS mesh operation from within the
Python environment.

"""
import os
import sys
import unittest
import smtk
import smtk.attribute
import smtk.mesh
import smtk.model
import smtk.session.discrete
import smtk.testing

import smtkxmsmesh

class GenerateFaceMesh(smtk.testing.TestCase):

    def setUp(self):
        # Set up the path to the test's input file
        modelFile = os.path.join(smtk.testing.DATA_DIR, 'test2D.2dm')

        # Load the input file
        print(modelFile)
        loadOp = smtk.session.discrete.ImportOperation.create()
        loadOp.parameters().find('filename').setValue(modelFile)
        loadRes = loadOp.operate()

        # Access the resource
        self.resource = smtk.model.Resource.CastTo(
            loadRes.find('resource').value())

        # Access the model
        self.model = loadRes.find('created').value()

    def testGenerateFaceMesh(self):
        # Create the "Generate Mesh" operator
        generateMeshOp = smtk.mesh.xms.GenerateMesh.create()

        # Associate it to the loaded model
        generateMeshOp.parameters().associate(self.model)

        # Execute the operation
        res = generateMeshOp.operate()

        # Test for success
        if res.find('outcome').value() != int(smtk.operation.Operation.Outcome.SUCCEEDED):
            raise Exception('"generate mesh" operator failed')

        # Print an overview of the output mesh
        meshCollection = res.find('resource').value()
        print('meshes: %d\tcells: %d\tpoints: %d' % (meshCollection.meshes().size(),
                                                     meshCollection.cells().size(),
                                                     meshCollection.points().size()))
        print('3d cells: %d\t2d cells: %d\t1d cells: %d' %
                  (meshCollection.cells(smtk.mesh.Dims3).size(),
                   meshCollection.cells(smtk.mesh.Dims2).size(),
                   meshCollection.cells(smtk.mesh.Dims1).size()))


if __name__ == '__main__':
    smtk.testing.process_arguments()
    unittest.main()
