//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_mesh_xms_operators_GenerateMesh_h
#define pybind_smtk_mesh_xms_operators_GenerateMesh_h

#include <pybind11/pybind11.h>

#include "smtk/mesh/xms/operators/GenerateMesh.h"

#include "smtk/operation/XMLOperation.h"

namespace py = pybind11;

PySharedPtrClass< smtk::mesh::xms::GenerateMesh, smtk::operation::XMLOperation > pybind11_init_smtk_xms_GenerateMesh(py::module &m)
{
  PySharedPtrClass< smtk::mesh::xms::GenerateMesh, smtk::operation::XMLOperation > instance(m, "GenerateMesh");
  instance
    .def(py::init<>())
    .def(py::init<::smtk::mesh::xms::GenerateMesh const &>())
    .def("deepcopy", (smtk::mesh::xms::GenerateMesh & (smtk::mesh::xms::GenerateMesh::*)(::smtk::mesh::xms::GenerateMesh const &)) &smtk::mesh::xms::GenerateMesh::operator=)
    .def_static("create", (std::shared_ptr<smtk::mesh::xms::GenerateMesh> (*)()) &smtk::mesh::xms::GenerateMesh::create)
    .def_static("create", (std::shared_ptr<smtk::mesh::xms::GenerateMesh> (*)(::std::shared_ptr<smtk::mesh::xms::GenerateMesh> &)) &smtk::mesh::xms::GenerateMesh::create, py::arg("ref"))
    .def("shared_from_this", (std::shared_ptr<const smtk::mesh::xms::GenerateMesh> (smtk::mesh::xms::GenerateMesh::*)() const) &smtk::mesh::xms::GenerateMesh::shared_from_this)
    .def("shared_from_this", (std::shared_ptr<smtk::mesh::xms::GenerateMesh> (smtk::mesh::xms::GenerateMesh::*)()) &smtk::mesh::xms::GenerateMesh::shared_from_this)
    ;
  return instance;
}

#endif
