//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#ifndef __smtk_mesh_xms_EdgeMap_h
#define __smtk_mesh_xms_EdgeMap_h

#include "smtk/PublicPointerDefs.h"
#include <unordered_map>

#include "smtk/model/EntityRef.h"

#include <xmsmesh/meshing/MeMultiPolyMesherIo.h>

namespace smtk
{
namespace model
{
class Model;
}

namespace mesh
{
namespace xms
{

struct SMTKXMSMESH_EXPORT EntityRefHasher
{
  size_t operator()(const smtk::model::EntityRef& ref) const
  {
    return ref.entity().hash();
  }
};

typedef std::unordered_map<smtk::model::EntityRef, ::xms::VecPt3d, EntityRefHasher> EdgeMap;
}
}
}

#endif
