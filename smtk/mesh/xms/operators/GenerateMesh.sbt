<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the XMS "GenerateMesh" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Operator -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="Globals" Label="Global Options" BaseType="operation">
      <BriefDescription>
        Generate a mesh from a model.
      </BriefDescription>
      <AssociationsDef Name="model" NumberOfRequiredValues="1" LockType="Read">
        <MembershipMask>model</MembershipMask>
        <BriefDescription>The model to mesh</BriefDescription>
      </AssociationsDef>
      <ItemDefinitions>

        <!--********** Global Sizing ***********-->
        <Double Name="Sizing" Label="Global Sizing Constraint" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <BriefDescription>Global Mesh Size to be used</BriefDescription>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>

        <!--********** Edge Refinement Sources ***********-->
        <Group Name="EdgeRefinementSources" Label="Hard Edges"
         Optional="true" IsEnabledByDefault="false"
         Extensible="true" NumberOfRequiredGroups="0">
          <ItemDefinitions>
            <Component Name="Edge" NumberOfRequiredValues="0" Extensible="true">
              <BriefDescription>The edge over which to set a refinement source.</BriefDescription>
              <Accepts><Resource Name="smtk::model::Resource" Filter="edge"/></Accepts>
            </Component>
          </ItemDefinitions>
        </Group>

        <!--********** Vertex Refinement Sources ***********-->
        <Group Name="VertexRefinementSources" Label="Vertex Refinement Sources"
         Optional="true" IsEnabledByDefault="false"
         Extensible="true" NumberOfRequiredGroups="0">
          <ItemDefinitions>
            <Component Name="Vertex" NumberOfRequiredValues="0" Extensible="true">
              <BriefDescription>The vertex over which to set a refinement source.</BriefDescription>
              <Accepts><Resource Name="smtk::model::Resource" Filter="vertex"/></Accepts>
            </Component>
            <Double Name="Size" Label="Size" >
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Int Name="HardPointMode" Label="Mode">
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="Off">0</Value>
                <Value Enum="Cell Centered">1</Value>
                <Value Enum="Hard Point">2</Value>
              </DiscreteInfo>
            </Int>
          </ItemDefinitions>
        </Group>

        <!--********** Point Refinement Sources ***********-->
        <Group Name="PointRefinementSources" Label="Point Refinement Sources"
         Optional="true" IsEnabledByDefault="false"
         Extensible="true" NumberOfRequiredGroups="0">
          <ItemDefinitions>
            <Double Name="Point" Label="Point" NumberOfRequiredValues="3">
              <ComponentLabels>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="Size" Label="Size" >
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Int Name="HardPointMode" Label="Mode">
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="Off">0</Value>
                <Value Enum="Cell Centered">1</Value>
                <Value Enum="Hard Point">2</Value>
              </DiscreteInfo>
            </Int>
          </ItemDefinitions>
        </Group>


        <!--********** Reverse Orientation ***********-->
        <Void Name="ReverseLoops" Label="Reverse Loop Orientation" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="0">
          <BriefDescription>Specify if we should reverse the orientation of all loops when meshing</BriefDescription>
        </Void>

        <!--********** Edge Meshing Toggle ***********-->
        <Void Name="MeshEdges" Label="Mesh Model Edges"
              Optional="true" IsEnabledByDefault="true" AdvanceLevel="1"/>

        <!--********** Face Meshing Toggle ***********-->
        <Void Name="MeshFaces" Label="Mesh Model Faces"
              Optional="true" IsEnabledByDefault="true" AdvanceLevel="1"/>

        <Group Name="sizing field" Label="Sizing Field"
               NumberOfRequiredValues="0" Extensible="true" Optional="true">
          <BriefDescription>Pairs of model groups and sizing fields to inform the mesher of mesh element sizing constraints.</BriefDescription>
          <ItemDefinitions>
            <Component Name="elements" Label="Element(s)"
                       NumberOfRequiredValues="1" Extensible="true">
              <BriefDescription>The element (or group of elements) over which to set a sizing constraint value.</BriefDescription>
              <Accepts><Resource Name="smtk::model::Resource" Filter=""/></Accepts>
            </Component>
            <Double Name="mesh size" Label="Mesh Length Scale (m)">
              <BriefDescription>The length scale associated with the model element.</BriefDescription>
              <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>

      </ItemDefinitions>
    </AttDef>

    <AttDef Type="LocalMeshingControls" Unique="false" BaseType="" Abstract="1" />

    <!--********** Per Model Element Sizing  ***********-->
    <AttDef Type="Local Sizing" Label="Local Mesh Size" BaseType="LocalMeshingControls"
      Version="0" Unique="true" RootName="Local Mesh Size">
      <AssociationsDef Name="InstanceFlagsAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>cell|12</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Sizing" Label="Sizing Constraint" Version="0" NumberOfRequiredValues="1">
          <BriefDescription>Local sizing constraint that must be > 0.</BriefDescription>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Face Bias" Label="Face Refinement Rate" BaseType="LocalMeshingControls"
      Version="0" Unique="true" RootName="Refinement Rate">
      <AssociationsDef Name="InstanceFlagsAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>cell|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Bias" Label="Face Bias Rate - closer to 0 biased to the edge and closer to 1 biased towards the face size" Version="0" NumberOfRequiredValues="1">
          <BriefDescription>Factor that controls how fast to control refinement constraint that must be between 0 and 1.</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <!--********** Per Face Line Elements  ***********-->
    <AttDef Type="LineRefinementSource" Label="Line Refinement Source" BaseType="LocalMeshingControls"
      Version="0" Unique="false" RootName="Line Ref Source">
      <AssociationsDef Name="InstanceFlagsAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>cell|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Line Start" Label="Line Start" NumberOfRequiredValues="3">
          <ComponentLabels>
            <Label>x1</Label>
            <Label>y1</Label>
            <Label>z1</Label>
          </ComponentLabels>
          <DefaultValue>0.0</DefaultValue>
        </Double>

        <Double Name="Line End" Label="Line End" NumberOfRequiredValues="3">
          <ComponentLabels>
            <Label>x2</Label>
            <Label>y2</Label>
            <Label>z2</Label>
          </ComponentLabels>
          <DefaultValue>0.0</DefaultValue>
        </Double>

        <Double Name="Line Size" Label="Line Size" NumberOfRequiredValues="1">
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Validation" Label="Validation Options" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>

        <Double Name="MinimumRelativeLength" Label="Minimum Relative Length" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <BriefDescription>Minimum ratio of global sizing to the largest length scale of the model</BriefDescription>
          <DefaultValue>0.0005</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
          <RangeInfo>
            <Max Inclusive="true">1</Max>
          </RangeInfo>
        </Double>

        <Double Name="MaximumRelativeLength" Label="Maximum Relative Length" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <BriefDescription>Maximum ratio of global sizing to the largest length scale of the model</BriefDescription>
          <DefaultValue>0.5</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
          <RangeInfo>
            <Max Inclusive="true">1</Max>
          </RangeInfo>
        </Double>

      </ItemDefinitions>
    </AttDef>

    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(generate mesh)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::mesh::Resource"/>
          </Accepts>
        </Resource>
        <Component Name="mesh_created" NumberOfRequiredValues="1" Extensible="true">
          <Accepts><Resource Name="smtk::model::Resource" Filter=""/></Accepts>
        </Component>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <!--********** Workflow Views ***********-->
  <Views>
    <View Type="Attribute" Title="Local Sizing" CreateEntities="true" ModelEntityFilter="fe">
      <AttributeTypes>
        <Att Type="Local Sizing" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Face Refinement Rate" CreateEntities="true" ModelEntityFilter="fe">
      <AttributeTypes>
        <Att Type="Face Bias" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Line Refinement Sources" CreateEntities="true" ModelEntityFilter="fe">
      <AttributeTypes>
        <Att Type="LineRefinementSource" />
      </AttributeTypes>
    </View>
    <View Type="Instanced" Title="Global Meshing Controls">
      <InstancedAttributes>
        <Att Name="Globals" Type="Globals" />
      </InstancedAttributes>
    </View>
    <View Type="Group" Title="Local Meshing Controls" TabPosition="north">
      <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
      <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
      <Views>
        <View Title="Local Sizing" />
        <View Title="Face Refinement Rate" />
        <View Title="Line Refinement Sources" />
      </Views>
    </View>
    <View Type="Instanced" Title="Validation Controls">
      <InstancedAttributes>
        <Att Name="Validation" Type="Validation" />
      </InstancedAttributes>
    </View>
    <View Type="Group" Title="Meshing Parameters" TopLevel="true" Style="Tiled">
      <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
      <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
      <Views>
        <View Title="Global Meshing Controls" />
        <View Title="Local Meshing Controls" />
        <View Title="Validation Controls" />
      </Views>
    </View>
  </Views>
</SMTK_AttributeResource>
