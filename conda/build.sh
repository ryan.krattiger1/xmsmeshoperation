#!/bin/bash

# To build package from shell env (do NOT build from a conda env):
# > conda build meta.yaml -c defaults -c conda-forge -c aquaveo -c johnkit --python=3.6
# Currently, we only have xmsmesh and smtk packages on python 3.6

conda config --add channels aquaveo
conda config --add channels johnkit
conda config --set show_channel_urls true

# Create build directory
start_dir=`pwd`
build_dir=${start_dir}/build
mkdir -p ${build_dir}
cd ${build_dir}

cmake \
  -DBoost_NO_SYSTEM_PATHS=ON \
  -DBOOST_INCLUDEDIR=$PREFIX/include \
  -DBOOST_LIBRARYDIR=$PREFIX/lib \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=$PREFIX \
  -DCMAKE_OSX_DEPLOYMENT_TARGET=10.9 \
  -DCMAKE_SYSROOT=$CONDA_BUILD_SYSROOT \
  -DENABLE_PYTHON_WRAPPING=ON \
  -DPYTHON_EXECUTABLE=$PYTHON \
  ${SRC_DIR}/src
cmake --build . -j "${CPU_COUNT}" --target install
